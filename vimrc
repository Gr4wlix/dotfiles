" Set encoding for this file
set encoding=utf-8
scriptencoding utf-8

" Set Leader key
let mapleader = " "

" Disable audible bell because it's annoying.
set noerrorbells visualbell t_vb=

" make 'Y' yank from cursor to end of line
nnoremap Y y$

" in Visual mode, yank into X clipboard using <Leader>y
" use :version to ensure vim can access the system clipboard
" You should see +clipboard listed (rather than -clipboard)
vnoremap <Leader>y "+y<Esc>

" Make <C-Space> start filename completion (omni-completion in file mode)
" It seems that <C-Space> is interpreted as <C-@> by terminal.app
inoremap <C-@> <C-x><C-f>
"
" Turn on syntax highlighting.
syntax on

" Show line numbers
set number
set relativenumber
" offset current line number from relative line numbers
set numberwidth=5

" Always show the status line at the bottom, even if you only have one window open.
set laststatus=2

" By default, Vim doesn't let you hide a buffer (i.e. have a buffer that isn't
" shown in any window) that has unsaved changes. This is to prevent you from "
" forgetting about unsaved changes and then quitting e.g. via `:qa!`. We find
" hidden buffers helpful enough to disable this protection. See `:help hidden`
" for more information on this.
set hidden

" Make searches ignore case
set ignorecase
" .. unless the searchterm includes a capital letter
set smartcase
" Enable searching as you type, rather than waiting until you press enter.
set incsearch
" highlight all results when searching
set hlsearch
" but disable highlighting on loading of a file
nohlsearch
" clear highlighting on double escape
nnoremap <silent> <Esc><Esc> :nohlsearch<CR>

" width of tab character (default = 8)
set tabstop=4
" indent width of '>', '<' and autoindent '='
set shiftwidth=4
" replace tab with spaces
"set expandtab
" simulates tab stops to be width of 4
set softtabstop=4
" more details about indentation settings here: https://stackoverflow.com/a/1878983/20137384

" Lines with equal indent form a fold.
set foldmethod=indent
" maximum nesting of folds
set foldnestmax=10
" open all folds when loading a file
set nofoldenable

" highlight the cursor
set cursorcolumn
set cursorline

" show vertical bar at 80 characters
set textwidth=80
set colorcolumn=+1

" Allow moving windows with single shortcut
noremap <C-j> <C-w>j
noremap <C-k> <C-w>k
noremap <C-h> <C-w>h
noremap <C-l> <C-w>l

" display extra whitespaces (not just regular space)
set list listchars=tab:»·,trail:·,nbsp:·

" when creating a new split, open new pane to right/bottom
set splitbelow
set splitright

" Use a line cursor within insert mode and a block cursor everywhere else.
" Reference chart of values:
"   Ps = 0  -> blinking block.
"   Ps = 1  -> blinking block (default).
"   Ps = 2  -> steady block.
"   Ps = 3  -> blinking underline.
"   Ps = 4  -> steady underline.
"   Ps = 5  -> blinking bar (xterm).
"   Ps = 6  -> steady bar (xterm).
let &t_SI = "\e[6 q"
let &t_EI = "\e[2 q"

" enable mouse navigation
set mouse=a

" Switch between last two files with <Leader><Leader>
nnoremap <Leader><Leader> <C-^>

" 'Q' in normal mode enters Ex mode. You almost never want this so remove the
" key binding
nmap Q <Nop>


" Install vim-plug if not found
if empty(glob('~/.vim/autoload/plug.vim'))
  silent !curl -fLo ~/.vim/autoload/plug.vim --create-dirs
    \ https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim
endif

" Run PlugInstall if there are missing plugins
autocmd VimEnter * if len(filter(values(g:plugs), '!isdirectory(v:val.dir)'))
  \| PlugInstall --sync | source $MYVIMRC
\| endif

" load plugins with vim-plug
call plug#begin('~/.vim/plugged')
    " help for vim-plug
    Plug 'junegunn/vim-plug'

    " colorscheme
    Plug 'ghifarit53/tokyonight-vim'

    " sensible vim defaults according to tpope
    Plug 'tpope/vim-sensible'

    " A simple, easy-to-use Vim alignment plugin
    Plug 'junegunn/vim-easy-align'

    " file explorer
    "Plug 'preservim/nerdtree'

    " Comments stuff out
    Plug 'tpope/vim-commentary'

    " Show file modifications in gutter
    Plug 'mhinz/vim-signify'

    " fuzzy file finder
    Plug 'ctrlpvim/ctrlp.vim'

	" easy motions
	"Plug 'easymotion/vim-easymotion'

	" incremental search easy motions
	"Plug 'haya14busa/incsearch.vim'
	"Plug 'haya14busa/incsearch-easymotion.vim'

	" code completion
	Plug 'neoclide/coc.nvim', {'branch': 'release'}
	"
    " run build and test suite commands asynchronously
    Plug 'tpope/vim-dispatch'

	Plug 'leafOfTree/vim-matchtag'

call plug#end()

" find colorschemes here: https://vimcolorschemes.com/
colorscheme tokyonight

" configure ctrlp launching shortcuts
nnoremap <Leader>o :CtrlP<CR> " start file search
nnoremap <Leader>b :CtrlPBuffer<CR> " start buffer search
nnoremap <Leader>r :CtrlPMRU<CR> " start recently used file search

" Start interactive EasyAlign in visual mode (e.g. vipga)
xmap ga <Plug>(EasyAlign)
" Start interactive EasyAlign for a motion/text object (e.g. gaip)
nmap ga <Plug>(EasyAlign)

" call this magic incantation to make plugins work, i guess?
filetype plugin indent on

" I use this to split paragraphs of text into lines ending in either . ! or ?.
" Also removes Trailing whitespace
nnoremap <Leader>= :s;[\.!?]\s\s*;&\r;g<CR> \| :nohl<CR>
vnoremap <Leader>= :s;.\zs\n;;<CR> \| :%s;\n;\r\r;<CR> \| :%s;[\.!?]\s\s*;&\r;g<CR> \| :nohl<CR>

" Remove trailing whitespace when saving
" https://vim.fandom.com/wiki/Remove_unwanted_spaces#Automatically_removing_all_trailing_whitespace
autocmd BufWritePre * :%s/\s\+$//e

" CoC remaps
" convirm completion with <CR>
inoremap <expr> <CR> coc#pum#visible() ? coc#pum#confirm() : "\<CR>"
" make <CR> select first component and confirm if no item selected
inoremap <silent><expr> <CR> coc#pum#visible() ? coc#_select_confirm() : "\<C-g>u\<CR>"

" trigger background builds
nnoremap <Leader>m :Make!<CR>
nnoremap <Leader>d :Make! diff<CR>
